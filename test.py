from Connect43D import Connect43D
from nn_Connect43D import Evaluator
# from alphazero import Alphazero
from mcts_agent.agent import AgentMCTS
from mcts_agent.mcts import apply_mcts
import random
# from main import multiple_against_random, multiple_against_mcts

board_z = 1
board_y = 3
board_x = 3
length = 3

# MCTS VS Random
def mcts_random(first_player):
    game = Connect43D(board_z, board_y, board_x, length)
    game.actual_player = first_player

    while not game.is_finished():
        if game.actual_player == 1:
            action = random.choice(game.get_move())
        else:
            action = apply_mcts(None, game, 100)

        r = game.do_move(action)

        print(game.game)

    return game.winner

def multiple_mcts_random(iteration=1):
    # mcts second
    winners = [mcts_random(1) for _ in range(iteration)]

    mcts_score = 0
    random_score = 0
    draw = 0

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 2:
            mcts_score += 1
        elif w == 1:
            random_score += 1
    
    # mcts first
    winners = [mcts_random(2) for _ in range(iteration)]

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 2:
            mcts_score += 1
        elif w == 1:
            random_score += 1

    print("random victories : {}".format(random_score))
    print("mcts victories : {}".format(mcts_score))
    print("draw : {}".format(draw))

# MCTS VS MCTS
def mcts_mcts():
    game = Connect43D(board_z, board_y, board_x, length)

    mcts_1 = AgentMCTS(1, game)
    mcts_2 = AgentMCTS(2, game)

    while not game.is_finished():
        if game.actual_player == 1:
            action = mcts_1.next_action(100)
            game.do_move(action)
        else:
            action = mcts_2.next_action(1000)
            game.do_move(action)

    print(game.game)

    return game.winner

def multiple_mcts_mcts(iteration=100):
    # mcts second
    winners = [mcts_mcts() for _ in range(iteration)]

    mcts_1 = 0
    mcts_2 = 0
    draw = 0

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 2:
            mcts_2 += 1
        elif w == 1:
            mcts_1 += 1
    
    print("mcts_1 victories : {}".format(mcts_1))
    print("mcts_2 victories : {}".format(mcts_2))  
    print("draw : {}".format(draw))

# multiple_mcts_random()
# multiple_mcts_mcts()

print(mcts_random(1))