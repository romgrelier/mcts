from node import Node

from copy import deepcopy

import numpy as np


class MCTS:
    def __init__(self, nn):
        self.root = Node()
        self.nn = nn

    def reset(self):
        self.root = Node()

    def search(self, init_state, self_play, iteration=50, temp=1):
        self.reset()

        for iter in range(iteration):
            node = self.root
            state = deepcopy(init_state)

            while not node.is_leaf():
                node = node.select()
                state.do_move(node.move)

            if not state.is_finished():
                p, v = node.evaluate(state, self.nn)
                v = -v
                p = np.exp(p)

                # removes not valid moves
                action_space = state.get_action_space()
                available_moves = state.get_move()

                for i in range(len(p)):
                    if action_space[i] not in available_moves:
                        p[i] = 0.0
                p /= sum(p)

                node.expand(state, p, self_play)
            else:
                if state.winner == 0:
                    v = 0.0
                else:
                    v = 1.0

            # backpropagation
            while node != None:
                node.backpropagate(v)
                node = node.parent
                v = -v

        pi = []
        available_moves = init_state.get_move()
        action_space = init_state.get_action_space()

        for i in range(len(action_space)):
            if action_space[i] in available_moves:
                for c in self.root.children:
                    if c.move == action_space[i]:
                        pi.append(c.n)
            else:
                pi.append(0.0)

        pi = np.array([n**(1.0 / temp) for n in pi])

        return pi / np.sum(pi)
