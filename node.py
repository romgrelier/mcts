from math import log
from math import sqrt

import numpy as np

class Node:
    def __init__(self, p=0.0, move=None, parent=None):
        self.parent = parent
        self.children = []

        self.n = 0.0  # visit count
        self.w = 0.0  # total action-value
        self.q = 0.0  # mean action-value
        self.p = p  # move probability

        self.move = move

    def _u(self, c=1.0):
        return c * self.p * (sqrt(self.parent.n)/(1 + self.n))

    def _c(self, c_init=1, c_base=1):
        return log((1 + self.n + c_base) / c_base) + c_init
    
    def _apply_dirichlet_noise(self, p, epsilon=0.25):
        """ attempt to apply dirichlet noise for exploration (only for the root node in self play) """
        noise = np.random.gamma(0.3, 1, len(p)) 

        return (1 - epsilon) * p.cpu().numpy() + epsilon * noise

    def is_leaf(self):
        return self.children == []

    def is_root(self):
        return self.parent == None

    def select(self, c=1.0):
        """ returns the best child of this node with PUCT """

        return sorted(self.children, key=lambda child: child.q + child._u(c))[-1]

    def expand(self, state, p, self_play):
        """ add all the children at once with their probability """
        action_space = state.get_action_space()
        available_moves = state.get_move()

        if self.is_root() and self_play:
            p = self._apply_dirichlet_noise(p)

        for i in range(len(p)):
            if action_space[i] in available_moves:
                new_child = Node(p[i], action_space[i], self)
                self.children.append(new_child)

    def evaluate(self, state, nn):
        """ evaluate the node and returns probability of the children and winning chance """

        return nn.predict(state.get_state(state.actual_player))

    def backpropagate(self, v):
        """ update the visit count and the values of the node """
        self.n += 1
        self.w += v
        self.q = self.w / self.n
