from mcts import MCTS
from node import Node

from random import uniform
import numpy as np

class Alphazero:
    def __init__(self, game, nn):
        self.game = game
        self.nnet = nn
        self.mcts = MCTS(self.nnet)
        self.iteration = 0

    def next_probabilities_value(self, self_play, viz=False):
        """
            build pi through a mcts
        """
        return self.mcts.search(self.game, self_play, temp=1.0)

    def next_action(self, pi):
        action = np.random.choice(len(pi), p=pi)

        return self.game.get_action_space()[action]

    def argmax_action(self, pi):
        action = np.argmax(pi)

        return self.game.get_action_space()[action]

    def update_action(self, action):
        """ update the root tree, if the child doesn't exist it will be created """
        new_root = None
        for c in self.mcts.root.children:
            if c.move == action:
                new_root = c
                new_root.parent = None
        if new_root == None:
            new_root = Node()
        
        self.mcts.root = new_root
        self.iteration += 1
