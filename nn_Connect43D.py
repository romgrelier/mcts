from torch import nn
from torch import tanh
from torch import sum
from torch import log
from torch import FloatTensor
import torch
import torch.nn.functional as F
import torch.optim as optim
import numpy as np
from tqdm import tqdm

class Evaluator(nn.Module):
    def __init__(self, z, y, x, num_channels=64):

        super(Evaluator, self).__init__()

        self.size_z = z
        self.size_y = y
        self.size_x = x
        self.num_channels = num_channels

        # 3D
        self.conv1 = nn.Conv3d(1, num_channels, 3, padding=1)
        self.conv2 = nn.Conv3d(num_channels, num_channels, 3, padding=1)
        self.conv3 = nn.Conv3d(num_channels, num_channels, 3, padding=1)
        self.conv4 = nn.Conv3d(num_channels, num_channels, 3, padding=1)

        self.bn1 = nn.BatchNorm3d(num_channels)
        self.bn2 = nn.BatchNorm3d(num_channels)
        self.bn3 = nn.BatchNorm3d(num_channels)
        self.bn4 = nn.BatchNorm3d(num_channels)

        # 2D
        # self.conv1 = nn.Conv2d(1, num_channels, 3, padding=1)
        # self.conv2 = nn.Conv2d(num_channels, num_channels, 3, padding=1)
        # self.conv3 = nn.Conv2d(num_channels, num_channels, 3, padding=1)
        # self.conv4 = nn.Conv2d(num_channels, num_channels, 3, padding=1)

        # self.bn1 = nn.BatchNorm2d(num_channels)
        # self.bn2 = nn.BatchNorm2d(num_channels)
        # self.bn3 = nn.BatchNorm2d(num_channels)
        # self.bn4 = nn.BatchNorm2d(num_channels)

        self.fc1 = nn.Linear(num_channels * self.size_z * self.size_y * self.size_x, 2048)
        self.fc_bn1 = nn.BatchNorm1d(2048)

        self.fc2 = nn.Linear(2048, 1024)
        self.fc_bn2 = nn.BatchNorm1d(1024)

        self.fc3 = nn.Linear(1024, 768)
        self.fc_bn3 = nn.BatchNorm1d(768)

        self.fc4 = nn.Linear(768, 512)
        self.fc_bn4 = nn.BatchNorm1d(512)

        # self.fc3 = nn.Linear(512, 256)
        self.fc_pi_1 = nn.Linear(512, 256)
        self.fc_bn_pi_1 = nn.BatchNorm1d(256)
        self.fc_pi_2 = nn.Linear(256, self.size_y * self.size_y)

        self.fc_v = nn.Linear(512, 1)

    def forward(self, s):
        # 3D
        s = s.view(-1, 1, self.size_z, self.size_y, self.size_x)
        # 2D
        # s = s.view(-1, 1, self.size_y, self.size_x)
        
        s = F.relu(self.bn1(self.conv1(s)))
        s = F.relu(self.bn2(self.conv2(s)))
        s = F.relu(self.bn3(self.conv3(s)))
        s = F.relu(self.bn4(self.conv4(s)))

        s = s.view(-1, self.num_channels * self.size_z *
                   self.size_y * self.size_x)

        s = F.dropout(F.relu(self.fc_bn1(self.fc1(s))), p=0.3,
                      training=self.training)
        s = F.dropout(F.relu(self.fc_bn2(self.fc2(s))), p=0.3,
                      training=self.training)
        s = F.dropout(F.relu(self.fc_bn3(self.fc3(s))), p=0.3,
                      training=self.training)
        s = F.dropout(F.relu(self.fc_bn4(self.fc4(s))), p=0.3,
                      training=self.training)

        pi = F.dropout(F.relu(self.fc_bn_pi_1(self.fc_pi_1(s))), p=0.3,
                      training=self.training)
        pi = self.fc_pi_2(pi)

        v = self.fc_v(s)

        #return F.log_softmax(pi, dim=1), tanh(v)
        return F.log_softmax(pi, dim=1), v
    
    def predict(self, observation):
        board = torch.FloatTensor(observation.astype(np.float64))
        board = board.view(1, observation.shape[0], observation.shape[1], observation.shape[2])
        
        board = board.cuda("cuda:0")
        #board = board.cuda()

        self.eval()
        with torch.no_grad():
            p, v = self(board)
        
        p = p.view(observation.shape[1] * observation.shape[2]).cpu()
        v = v.view(1).cpu()

        return p, v


    def train_model(self, examples, epoch=50, batch_size=32, device="cuda:0"):
        self.train()

        optimizer = optim.Adam(self.parameters(), lr=0.001)

        total_pi_loss = 0
        total_v_loss = 0
        i = 1

        pbar = tqdm(range(epoch))
        for e in pbar:
            batch_id = 0
            while batch_id < int(len(examples)/batch_size):
                sample_ids = np.random.randint(len(examples), size=batch_size)
                boards, pis, vs = list(zip(*[examples[i] for i in sample_ids]))

                boards = FloatTensor(np.array(boards).astype(np.float64))
                boards = boards.view(-1, self.size_z, self.size_y, self.size_x)

                target_pis = FloatTensor(np.array(pis))
                target_vs = FloatTensor(np.array(vs).astype(np.float64))

                boards, target_pis, target_vs = boards.contiguous().cuda(
                    device), target_pis.contiguous().cuda(device), target_vs.contiguous().cuda(device)

                #boards, target_pis, target_vs = boards.contiguous().cuda(), target_pis.contiguous().cuda(), target_vs.contiguous().cuda()

                out_pi, out_v = self(boards)
                l_pi = loss_pi(target_pis, out_pi)
                l_v = loss_v(target_vs, out_v)
                total_loss = l_pi + l_v

                total_pi_loss += l_pi.item()
                total_v_loss += l_v.item()

                optimizer.zero_grad()
                total_loss.backward()
                optimizer.step()

                batch_id += 1
                i += 1

                # pbar.set_postfix(l_pi=l_pi.item(), l_v=l_v.item())
                pbar.set_postfix(l_pi=total_pi_loss / i, l_v=total_v_loss / i)


def loss_pi(targets, outputs):
    return -sum(targets * outputs) / targets.size()[0]


def loss_v(targets, outputs):
    return sum((targets - outputs.view(-1))**2) / targets.size()[0]
