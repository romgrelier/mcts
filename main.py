from nn_Connect43D import Evaluator
from alphazero import Alphazero
from Connect43D import Connect43D
from mcts_agent.agent import AgentMCTS

import sys
from random import shuffle, choice
from copy import deepcopy
import random

from joblib import Parallel, delayed
import numpy as np
import torch
from tqdm import tqdm

board_z = 4
board_y = 4
board_x = 4
length = 4
max_jobs = 8

total_new_model_score = []
total_old_model_score = []

total_avs_alphazero_score = []
total_avs_random_score = []

total_avm_alphazero_score = []
total_avm_mcts_score = []

def run_episode(model):

    game = Connect43D(board_z, board_y, board_x, length)
    agent = Alphazero(game, model)

    examples = []

    while not game.is_finished():
        pi = agent.next_probabilities_value(self_play=True)

        # print("pi")
        # print(pi)

        # Data augmentation with rotation of the board
        for rot in range(4):
            matrix_state = np.rot90(game.get_state(
                game.actual_player), rot, axes=(1, 2))

            # rotate pi
            matrix_pi = pi.reshape((board_y, board_x))
            matrix_pi_rot = np.rot90(matrix_pi, rot)
            vector_pi = matrix_pi_rot.reshape(board_x * board_y)

            # add the data to the examples set
            examples.append(
                np.array([matrix_state, vector_pi, game.actual_player])
            )
    
        # for rot in range(4):
        #     matrix_state = np.rot90(game.get_state(
        #         3 - game.actual_player), rot, axes=(1, 2))

        #     # rotate pi
        #     matrix_pi = pi.reshape((board_y, board_x))
        #     matrix_pi_rot = np.rot90(matrix_pi, rot)
        #     vector_pi = matrix_pi_rot.reshape(board_x * board_y)

        #     # add the data to the examples set
        #     examples.append(
        #         np.array([matrix_state, vector_pi, 3 - game.actual_player])
        #     )

        # apply the action
        action = agent.next_action(pi)
        r = game.do_move(action)
        # agent.update_action(action)
    
    # print(game.game)

    for e in examples:
        if game.winner == 0:
            e[2] = 0.0
        elif e[2] == game.winner:
            e[2] = 1.0
        else :
            e[2] = -1.0

    return examples


def generate_examples(model, size=128, jobs=max_jobs):
    pbar = tqdm(range(size))

    examples_output = Parallel(jobs, "loky")(
        delayed(run_episode)(model) for _ in pbar)
    
    # examples_output = [run_episode(model) for _ in range(size)]

    examples = []
    for e in examples_output:
        examples.extend(e)

    return examples

def run_game(model_1, model_2):
    game = Connect43D(board_z, board_y, board_x, length)

    agent_1 = Alphazero(game, model_1)
    agent_2 = Alphazero(game, model_2)

    while not game.is_finished():
        if game.actual_player == 1:
            pi = agent_1.next_probabilities_value(self_play=False)
            # print(np.argmax(pi))
            action = agent_1.next_action(pi)
            # action = agent_1.argmax_action(pi)
            r = game.do_move(action)

            # agent_1.update_action(action)
            # agent_2.update_action(action)
        else:
            pi = agent_2.next_probabilities_value(self_play=False)
            # print(np.argmax(pi))
            action = agent_2.next_action(pi)
            # action = agent_2.argmax_action(pi)
            r = game.do_move(action)

            # agent_1.update_action(action)
            # agent_2.update_action(action)

    # print(game.game)

    return game.winner


def versus(old_model, new_model, iteration=25):
    total_new_model = 0
    total_old_model = 0
    total_draw = 0

    print("=== new model as player 1 ===")
    pbar = tqdm(range(iteration))
    winners = Parallel(max_jobs, "loky")(delayed(run_game)(
       new_model, old_model) for _ in pbar)

    # winners = [run_game(new_model, old_model) for _ in range(int(iteration))]

    old_model_victory = 0
    new_model_victory = 0
    draw = 0

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 1:
            new_model_victory += 1
        elif w == 2:
            old_model_victory += 1

    print("new model victories : {}".format(new_model_victory))
    print("old model victories : {}".format(old_model_victory))
    print("draw : {}".format(draw))

    total_new_model += new_model_victory
    total_old_model += old_model_victory
    total_draw += draw

    print("=== new model as player 2 ===")
    pbar = tqdm(range(iteration))
    winners = Parallel(max_jobs, "loky")(delayed(run_game)(
        old_model, new_model) for _ in pbar)

    # winners = [run_game(old_model, new_model) for _ in range(int(iteration))]

    old_model_victory = 0
    new_model_victory = 0
    draw = 0

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 2:
            new_model_victory += 1
        elif w == 1:
            old_model_victory += 1
    
    print("new model victories : {}".format(new_model_victory))
    print("old model victories : {}".format(old_model_victory))
    print("draw : {}".format(draw))

    total_new_model += new_model_victory
    total_old_model += old_model_victory
    total_draw += draw
    
    total_new_model_score.append(total_new_model)
    total_old_model_score.append(total_old_model)


    print("total new model victories : {}".format(total_new_model))
    print("total old model victories : {}".format(total_old_model))
    print("total draw : {}".format(total_draw))

    if total_new_model >= total_old_model:
        print("new model acepted")
        return new_model, True
    else:
        print("new model rejected")
        return old_model, False

def against_random(model, second=False):
    game = Connect43D(board_z, board_y, board_x, length)
    if not second:
        game.actual_player = 2

    alphazero = Alphazero(game, model)

    while not game.is_finished():
        if game.actual_player == 1:
            action = random.choice(game.get_move())
            r = game.do_move(action)

            # alphazero.update_action(action)
        else:
            pi = alphazero.next_probabilities_value(self_play=False)
            action = alphazero.argmax_action(pi)
            r = game.do_move(action)

            # alphazero.update_action(action)
    
    # print(game.game)

    return game.winner

def multiple_against_random(model, iteration=50):
    print("=== against random as player 1 ===")
    pbar = tqdm(range(iteration))
    winners = Parallel(max_jobs, "loky")(delayed(against_random)(
    model, second=True) for _ in pbar)

    # winners = [against_random(model, second=False) for _ in range(iteration)]

    alphazero_score = 0
    random_score = 0
    draw = 0

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 2:
            alphazero_score += 1
        elif w == 1:
            random_score += 1

    print("alphazero victories : {}".format(alphazero_score))       
    print("random victories : {}".format(random_score))   
    print("draw : {}".format(draw))

    print("=== against random as player 2 ===")
    pbar = tqdm(range(iteration))
    winners = Parallel(max_jobs, "loky")(delayed(against_random)(
    model) for _ in pbar)

    # winners = [against_random(model, second=True) for _ in range(iteration)]

    alphazero_score = 0
    random_score = 0
    draw = 0

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 2:
            alphazero_score += 1
        elif w == 1:
            random_score += 1

    total_avs_alphazero_score.append(alphazero_score)
    total_avs_random_score.append(random_score)

    print("alphazero victories : {}".format(alphazero_score))
    print("random victories : {}".format(random_score))  
    print("draw : {}".format(draw))

def against_mcts(model, second=False):
    game = Connect43D(board_z, board_y, board_x, length)
    if not second:
        game.actual_player = 2

    alphazero = Alphazero(game, model)
    mcts = AgentMCTS(1, game)

    while not game.is_finished():
        if game.actual_player == 1:
            action = mcts.next_action(600)
            r = game.do_move(action)

            # alphazero.update_action(action)
            # mcts.update_action(action)
        else:
            pi = alphazero.next_probabilities_value(self_play=False)

            action = alphazero.argmax_action(pi)
            r = game.do_move(action)

            # alphazero.update_action(action)
            # mcts.update_action(action)

    # print(game.game)

    return game.winner

def multiple_against_mcts(model, iteration=50):
    print("=== against mcts as player 1 ===")
    pbar = tqdm(range(iteration))
    winners = Parallel(max_jobs, "loky")(delayed(against_mcts)(
    model, second=True) for _ in pbar)

    # winners = [against_random(model, second=False) for _ in range(iteration)]

    alphazero_score = 0
    random_score = 0
    draw = 0

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 2:
            alphazero_score += 1
        elif w == 1:
            random_score += 1

    print("alphazero victories : {}".format(alphazero_score))       
    print("mcts victories : {}".format(random_score))   
    print("draw : {}".format(draw))

    print("=== against mcts as player 2 ===")
    pbar = tqdm(range(iteration))
    winners = Parallel(max_jobs, "loky")(delayed(against_mcts)(
    model) for _ in pbar)

    # winners = [against_random(model, second=True) for _ in range(iteration)]

    alphazero_score = 0
    random_score = 0
    draw = 0

    for w in winners:
        if w == 0:
            draw += 1
        elif w == 2:
            alphazero_score += 1
        elif w == 1:
            random_score += 1
    
    total_avm_alphazero_score.append(alphazero_score)
    total_avm_mcts_score.append(random_score)


    print("alphazero victories : {}".format(alphazero_score))
    print("mcts victories : {}".format(random_score))  
    print("draw : {}".format(draw))

def main():

    model = Evaluator(board_z, board_y, board_x).cuda("cuda:0")
    # model = torch.load("model.save").cuda()
    #model = Evaluator(board_z, board_y, board_x)
    #model = torch.nn.DataParallel(model)

    epoch = 20
    max_examples = 4
    examples = []

    for e in range(epoch):

        print("Epoch {}".format(e))
        print("Building examples...")
        examples.append(generate_examples(model))

        # for ex in examples:
        #     print("game : ")
        #     for e in ex:
        #         print(e)

        if len(examples) > max_examples:
            examples.pop(0)

        train_examples = []
        for ex in examples:
            train_examples.extend(ex)
        shuffle(train_examples)

        print("Training...")
        old_model = deepcopy(model)
        model.train_model(train_examples)

        print("VS...")
        model, result = versus(old_model, model)
        #if not result:
        #    examples = examples[:-1]

        # print("VS random")
        # multiple_against_random(model)

        # print("VS mcts")
        # multiple_against_mcts(model)

        print("Saving...")
        torch.save(model, "model_connect_{}_{}_{}_{}_{}.save".format(board_z, board_y, board_x, length, "linear_small"))

        # print("new model victories {}".format(total_new_model_score))
        # print("old model victories {}".format(total_old_model_score))

        # print("alphazero victories {}".format(total_avs_alphazero_score))
        # print("random victories {}".format(total_avs_random_score))

        # print("alphazero victories {}".format(total_avm_alphazero_score))
        # print("mcts victories {}".format(total_avm_mcts_score))
        
        # print("=========   =========   =========   =========   =========   =========   =========")
        # print("=========   =========   =========   =========   =========   =========   =========")



if __name__ == "__main__":
    main()
